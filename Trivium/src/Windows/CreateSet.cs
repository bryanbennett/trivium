﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Trivium.src.Data;

namespace Trivium.src.Windows
{
    public partial class CreateSet : Form
    {
        private Main m;
        private QuestionSet set;
        private Question currentQ;
        private string nameOfSet;
        private int qNum = 1;
        public CreateSet(Main m)
        {
            InitializeComponent();
            this.m = m;
            NameSet name = new NameSet(this);
            DialogResult dr = name.ShowDialog();

            if (dr == DialogResult.OK)
            {
                set = new QuestionSet(nameOfSet);
                currentQ = new Question(qNum);
            }
            else
            {
                this.Close();
            }
        }
        public CreateSet(Main m, QuestionSet set)
        {
            InitializeComponent();
            this.m = m;
            this.set = set;
            SetQuestion(1);
        }
        private void ResetControls()
        {
            rtbPrompt.Text = "Question Goes Here";
            rtbA.Text = "Answer A Goes Here";
            rtbB.Text = "Answer B Goes Here";
            rtbC.Text = "Answer C Goes Here";
            rtbD.Text = "Answer D Goes Here";
            radA.Checked = true;
            lblQuestionNumber.Text = "Question " + (qNum) + ":";
            currentQ = new Question(qNum);
        }
        private void SetQuestion(int num)
        {
            int count = set.Questions.Count;
            qNum = num;

            if (num <= count)
            {
                ResetControls();
                currentQ = set.Questions[num - 1];
                rtbPrompt.Text = currentQ.Prompt;

                string[] answers = currentQ.Answers;
                rtbA.Text = answers[0];
                rtbB.Text = answers[1];
                rtbC.Text = answers[2];
                rtbD.Text = answers[3];

                int answerIndex = currentQ.AnswerIndex;

                if (answerIndex == 1)
                {
                    radB.Checked = true;
                }
                if (answerIndex == 2)
                {
                    radC.Checked = true;
                }
                if (answerIndex == 3)
                {
                    radD.Checked = true;
                }
            }
            else
            {
                ResetControls();
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            SaveQuestion();
        }
        private void SaveQuestion()
        {
            List<Question> questions = set.Questions;
            Question q = new Question(qNum);

            bool oldQuestion = false;
            foreach (Question question in questions)
            {
                if (question.QuestionNumber == qNum)
                {
                    q = question;
                    oldQuestion = true;
                    break;
                }
            }
            q.Prompt = rtbPrompt.Text;

            string[] answers = new string[4];
            answers[0] = rtbA.Text;
            answers[1] = rtbB.Text;
            answers[2] = rtbC.Text;
            answers[3] = rtbD.Text;
            q.Answers = answers;

            int answerIndex = 0; //Assumes A

            if (radB.Checked)
            {
                answerIndex = 1;
            }
            if (radC.Checked)
            {
                answerIndex = 2;
            }
            if (radD.Checked)
            {
                answerIndex = 3;
            }
            q.AnswerIndex = answerIndex;

            if (oldQuestion)
            {
                qNum += 1;
                SetQuestion(qNum);
            }
            else
            {
                set.Questions.Add(q);
                qNum += 1;
                ResetControls();
            }
        }
        private void btnPrevious_Click(object sender, EventArgs e)
        {
            qNum -= 1;
            if (qNum < 1)
            {
                qNum = 1;
            }
            SetQuestion(qNum);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Do you wish to save the current question?", "Saving", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                SaveQuestion();
            }
            if (set.Questions.Count == 0)
            {
                MessageBox.Show("You must have at least one question in a question set.");
                return;
            }
            string path = m.QuestionSetPath;
            Stream setStream = File.Create(path + "\\" + set.ToString());
            BinaryFormatter serializer = new BinaryFormatter();
            serializer.Serialize(setStream, set);
            setStream.Close();
            MessageBox.Show("The question set " + nameOfSet + " has been saved.");
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            List<Question> questions = set.Questions;

            if (questions.Contains(currentQ))
            {
                questions.Remove(currentQ);

                int count = questions.Count;
                for (int i = 0; i < count; i++)
                {
                    Question question = questions[i];
                    question.QuestionNumber = i + 1;
                }
                SetQuestion(qNum);
            }
        }
        #region "Properties"
        /// <summary>
        /// Gets or sets the name of the QuestionSet.
        /// </summary>
        public string SetName { get { return nameOfSet; } set { nameOfSet = value; } }
        #endregion
    }
}
