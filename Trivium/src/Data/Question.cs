﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trivium.src.Data
{
    [Serializable()]
    public class Question
    {
        private string prompt = "N/A";
        private int questionNumber;
        private string[] answers;
        private int answerIndex = 0;

        public Question(int questionNumber)
        {
            this.questionNumber = questionNumber;
        }


        #region "Properties"
        /// <summary>
        /// Gets or sets the prompt to be displayed.
        /// </summary>
        public string Prompt { get { return prompt; } set { prompt = value; } }

        /// <summary>
        /// Gets or sets the answers to be displayed.  Index 0 = A, 1 = B, 2 = C, and 3 = D.
        /// </summary>
        public string[] Answers { get { return answers; } set { answers = value; } }

        /// <summary>
        /// Gets or sets the index of the string[] array that corresponds with the correct answer to the question.
        /// </summary>
        public int AnswerIndex { get { return answerIndex; } set { answerIndex = value; } }

        /// <summary>
        /// Returns the question number of this question.
        /// </summary>
        public int QuestionNumber { get { return questionNumber; } set { questionNumber = value; } }
        #endregion
    }
}
