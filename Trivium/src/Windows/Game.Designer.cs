﻿namespace Trivium.src.Windows
{
    partial class Game
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Game));
            this.btnX = new System.Windows.Forms.Button();
            this.btnMinimize = new System.Windows.Forms.Button();
            this.lblQuestionNumber = new System.Windows.Forms.Label();
            this.rtbPrompt = new System.Windows.Forms.RichTextBox();
            this.btnA = new System.Windows.Forms.Button();
            this.btnB = new System.Windows.Forms.Button();
            this.btnC = new System.Windows.Forms.Button();
            this.btnD = new System.Windows.Forms.Button();
            this.rtbA = new System.Windows.Forms.RichTextBox();
            this.rtbB = new System.Windows.Forms.RichTextBox();
            this.rtbC = new System.Windows.Forms.RichTextBox();
            this.rtbD = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // btnX
            // 
            this.btnX.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnX.Location = new System.Drawing.Point(729, 3);
            this.btnX.Name = "btnX";
            this.btnX.Size = new System.Drawing.Size(35, 23);
            this.btnX.TabIndex = 0;
            this.btnX.Text = "X";
            this.btnX.UseVisualStyleBackColor = true;
            this.btnX.Click += new System.EventHandler(this.btnX_Click);
            // 
            // btnMinimize
            // 
            this.btnMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinimize.Location = new System.Drawing.Point(688, 3);
            this.btnMinimize.Name = "btnMinimize";
            this.btnMinimize.Size = new System.Drawing.Size(35, 23);
            this.btnMinimize.TabIndex = 1;
            this.btnMinimize.Text = "__";
            this.btnMinimize.UseVisualStyleBackColor = true;
            this.btnMinimize.Click += new System.EventHandler(this.btnMinimize_Click);
            // 
            // lblQuestionNumber
            // 
            this.lblQuestionNumber.AutoSize = true;
            this.lblQuestionNumber.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuestionNumber.Location = new System.Drawing.Point(12, 9);
            this.lblQuestionNumber.Name = "lblQuestionNumber";
            this.lblQuestionNumber.Size = new System.Drawing.Size(139, 25);
            this.lblQuestionNumber.TabIndex = 3;
            this.lblQuestionNumber.Text = "Question N:";
            // 
            // rtbPrompt
            // 
            this.rtbPrompt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.rtbPrompt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbPrompt.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbPrompt.ForeColor = System.Drawing.SystemColors.Control;
            this.rtbPrompt.Location = new System.Drawing.Point(16, 47);
            this.rtbPrompt.Name = "rtbPrompt";
            this.rtbPrompt.ReadOnly = true;
            this.rtbPrompt.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbPrompt.Size = new System.Drawing.Size(739, 147);
            this.rtbPrompt.TabIndex = 4;
            this.rtbPrompt.Text = "N/A";
            // 
            // btnA
            // 
            this.btnA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnA.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnA.Location = new System.Drawing.Point(16, 217);
            this.btnA.Name = "btnA";
            this.btnA.Size = new System.Drawing.Size(75, 42);
            this.btnA.TabIndex = 5;
            this.btnA.Text = "A";
            this.btnA.UseVisualStyleBackColor = true;
            this.btnA.Click += new System.EventHandler(this.btnA_Click);
            // 
            // btnB
            // 
            this.btnB.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnB.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnB.Location = new System.Drawing.Point(17, 306);
            this.btnB.Name = "btnB";
            this.btnB.Size = new System.Drawing.Size(75, 42);
            this.btnB.TabIndex = 6;
            this.btnB.Text = "B";
            this.btnB.UseVisualStyleBackColor = true;
            this.btnB.Click += new System.EventHandler(this.btnB_Click);
            // 
            // btnC
            // 
            this.btnC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnC.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnC.Location = new System.Drawing.Point(17, 393);
            this.btnC.Name = "btnC";
            this.btnC.Size = new System.Drawing.Size(75, 42);
            this.btnC.TabIndex = 7;
            this.btnC.Text = "C";
            this.btnC.UseVisualStyleBackColor = true;
            this.btnC.Click += new System.EventHandler(this.btnC_Click);
            // 
            // btnD
            // 
            this.btnD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnD.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnD.Location = new System.Drawing.Point(17, 476);
            this.btnD.Name = "btnD";
            this.btnD.Size = new System.Drawing.Size(75, 42);
            this.btnD.TabIndex = 8;
            this.btnD.Text = "D";
            this.btnD.UseVisualStyleBackColor = true;
            this.btnD.Click += new System.EventHandler(this.btnD_Click);
            // 
            // rtbA
            // 
            this.rtbA.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.rtbA.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbA.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbA.ForeColor = System.Drawing.SystemColors.Control;
            this.rtbA.Location = new System.Drawing.Point(114, 217);
            this.rtbA.Name = "rtbA";
            this.rtbA.ReadOnly = true;
            this.rtbA.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbA.Size = new System.Drawing.Size(641, 80);
            this.rtbA.TabIndex = 9;
            this.rtbA.Text = "N/A";
            // 
            // rtbB
            // 
            this.rtbB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.rtbB.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbB.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbB.ForeColor = System.Drawing.SystemColors.Control;
            this.rtbB.Location = new System.Drawing.Point(115, 306);
            this.rtbB.Name = "rtbB";
            this.rtbB.ReadOnly = true;
            this.rtbB.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbB.Size = new System.Drawing.Size(641, 80);
            this.rtbB.TabIndex = 10;
            this.rtbB.Text = "N/A";
            // 
            // rtbC
            // 
            this.rtbC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.rtbC.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbC.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbC.ForeColor = System.Drawing.SystemColors.Control;
            this.rtbC.Location = new System.Drawing.Point(114, 392);
            this.rtbC.Name = "rtbC";
            this.rtbC.ReadOnly = true;
            this.rtbC.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbC.Size = new System.Drawing.Size(641, 80);
            this.rtbC.TabIndex = 11;
            this.rtbC.Text = "N/A";
            // 
            // rtbD
            // 
            this.rtbD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.rtbD.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbD.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbD.ForeColor = System.Drawing.SystemColors.Control;
            this.rtbD.Location = new System.Drawing.Point(115, 476);
            this.rtbD.Name = "rtbD";
            this.rtbD.ReadOnly = true;
            this.rtbD.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtbD.Size = new System.Drawing.Size(641, 80);
            this.rtbD.TabIndex = 12;
            this.rtbD.Text = "N/A";
            // 
            // Game
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(10)))), ((int)(((byte)(10)))));
            this.ClientSize = new System.Drawing.Size(768, 571);
            this.Controls.Add(this.rtbD);
            this.Controls.Add(this.rtbC);
            this.Controls.Add(this.rtbB);
            this.Controls.Add(this.rtbA);
            this.Controls.Add(this.btnD);
            this.Controls.Add(this.btnC);
            this.Controls.Add(this.btnB);
            this.Controls.Add(this.btnA);
            this.Controls.Add(this.rtbPrompt);
            this.Controls.Add(this.lblQuestionNumber);
            this.Controls.Add(this.btnMinimize);
            this.Controls.Add(this.btnX);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.Control;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Game";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Game";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnX;
        private System.Windows.Forms.Button btnMinimize;
        private System.Windows.Forms.Label lblQuestionNumber;
        private System.Windows.Forms.RichTextBox rtbPrompt;
        private System.Windows.Forms.Button btnA;
        private System.Windows.Forms.Button btnB;
        private System.Windows.Forms.Button btnC;
        private System.Windows.Forms.Button btnD;
        private System.Windows.Forms.RichTextBox rtbA;
        private System.Windows.Forms.RichTextBox rtbB;
        private System.Windows.Forms.RichTextBox rtbC;
        private System.Windows.Forms.RichTextBox rtbD;
    }
}