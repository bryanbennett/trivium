﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Trivium.src.Data;

namespace Trivium.src.Windows
{
    public partial class Game : Form
    {
        private QuestionSet set;
        private Main m;
        private List<Question> questions;
        private int currentIndex = 0;

        public Game(QuestionSet set, Main m)
        {
            InitializeComponent();
            this.m = m;
            this.set = set;
            questions = set.Questions;
            NextQuestion();
        }
        private void NextQuestion()
        {
            if (currentIndex < questions.Count)
            {
                lblQuestionNumber.Text = "Question " + (currentIndex + 1) + " of " + set.Questions.Count() + ":";
                Question q = questions[currentIndex];
                rtbPrompt.Text = q.Prompt;
                string[] answers = q.Answers;

                rtbA.Text = answers[0];
                rtbB.Text = answers[1];
                rtbC.Text = answers[2];
                rtbD.Text = answers[3];
            }
            else
            {
                MessageBox.Show("The game has concluded.");
                m.Show();
                this.Close();
            }
        }
        private void CheckAnswer(int index)
        {
            if (index == questions[currentIndex].AnswerIndex)
            {
                MessageBox.Show("Correct!");
                currentIndex += 1;
                NextQuestion();
            }
            else
            {
                MessageBox.Show("Incorrect!");
            }
        }
        #region "Answer Buttons"
        private void btnA_Click(object sender, EventArgs e)
        {
            CheckAnswer(0);
        }

        private void btnB_Click(object sender, EventArgs e)
        {
            CheckAnswer(1);
        }

        private void btnC_Click(object sender, EventArgs e)
        {
            CheckAnswer(2);
        }

        private void btnD_Click(object sender, EventArgs e)
        {
            CheckAnswer(3);
        }
        #endregion
        #region "Properties"
        #endregion
        #region "Close/Minimize"
        private void btnX_Click(object sender, EventArgs e)
        {
            m.Show();
            this.Close();
        }

        private void btnMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        #endregion
    }
}
