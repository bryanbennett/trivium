﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Trivium.src.Windows
{
    public partial class NameSet : Form
    {
        private CreateSet c;
        public NameSet(CreateSet c)
        {
            InitializeComponent();
            this.c = c;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string name = txtName.Text;
            name = name.Trim();
            if (name.Equals(""))
            {
                MessageBox.Show("You must enter in the name of the set.");
                return;
            }
            if (name.Contains("\\") || name.Contains("/"))
            {
                MessageBox.Show("Name must not have any slashes in the name.");
                return;
            }
            this.DialogResult = DialogResult.OK;
            c.SetName = name;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
