﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Trivium.src.Data
{
    [Serializable()]
    public class QuestionSet
    {
        private string name;
        private List<Question> questions;

        public QuestionSet(string name)
        {
            this.name = name;
            questions = new List<Question>();
        }
        public void AddQuestion(Question q)
        {
            questions.Add(q);
        }
        public void RemoveQuestion(Question q)
        {
            questions.Remove(q);
        }
        public void RemoveQuestion(int index)
        {
            questions.RemoveAt(index);
        }
        public override string ToString()
        {
            return name;
        }
        #region "Properties"
        /// <summary>
        /// Returns all the questions contained in this set.
        /// </summary>
        public List<Question> Questions { get { return questions; } }
        #endregion



    }
}
