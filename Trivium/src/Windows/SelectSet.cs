﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Trivium.src.Data;

namespace Trivium.src.Windows
{
    public partial class SelectSet : Form
    {
        private Main m;
        private bool gameMode = false;

        public SelectSet(Main m, bool gameMode)
        {
            InitializeComponent();
            this.m = m;
            BuildComboBox();
            this.gameMode = gameMode;
        }
        private void BuildComboBox()
        {
            cbxSets.Items.Add("Select a Question Set");
            string path = m.QuestionSetPath;
            string[] paths = Directory.GetFiles(path);
            for (int i = 0; i < paths.Length; i++)
            {
                cbxSets.Items.Add(Deserialize(paths[i]));
            }

            cbxSets.SelectedIndex = 0;
        }
        private QuestionSet Deserialize(string path)
        {
            Stream input = File.OpenRead(path);
            BinaryFormatter bf = new BinaryFormatter();
            QuestionSet set = (QuestionSet)bf.Deserialize(input);
            input.Close();
            return set;
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnBegin_Click(object sender, EventArgs e)
        {
            if (cbxSets.SelectedIndex == 0)
            {
                MessageBox.Show("You must select a question set from the drop down list.");
                return;
            }
            if (gameMode)
            {
                QuestionSet set = (QuestionSet)cbxSets.SelectedItem;
                m.SetQuestionSet(set);
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                QuestionSet set = (QuestionSet)cbxSets.SelectedItem;
                CreateSet cs = new CreateSet(m, set);
                cs.ShowDialog();
                cs.Dispose();
                this.Close();
            }
        }

        private void SelectSet_Load(object sender, EventArgs e)
        {

        }
    }
}
