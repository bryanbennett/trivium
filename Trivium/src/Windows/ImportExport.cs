﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Trivium.src.Data;

namespace Trivium.src.Windows
{
    public partial class ImportExport : Form
    {
        private Main m;
        private bool isExporting = true;
        private QuestionSet importedSet;
        public ImportExport(Main m)
        {
            InitializeComponent();
            this.m = m;
            BuildComboBox();
        }
        private void BuildComboBox()
        {
            cbxSets.Items.Add("Select a Question Set");
            string path = m.QuestionSetPath;
            string[] paths = Directory.GetFiles(path);
            for (int i = 0; i < paths.Length; i++)
            {
                cbxSets.Items.Add(Deserialize(paths[i]));
            }

            cbxSets.SelectedIndex = 0;
        }
        private QuestionSet Deserialize(string path)
        {
            Stream input = File.OpenRead(path);
            BinaryFormatter bf = new BinaryFormatter();
            QuestionSet set = (QuestionSet)bf.Deserialize(input);
            input.Close();
            return set;
        }


        private void btnSelect_Click(object sender, EventArgs e)
        {
            DialogResult dr = openFileDialog.ShowDialog();

            if (dr == DialogResult.OK)
            {
                string path = openFileDialog.FileName;
                try
                {
                    importedSet = Deserialize(path);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Invalid question set.  Please select a valid question site file.");
                    return;
                }
                isExporting = false;
                txtSet.Text = path;
                cbxSets.Enabled = false;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (isExporting)
            {
                Export();
            }
            else
            {
                Import();
            }
        }
        private void Export()
        {
            DialogResult dr = folderBrowserDialog.ShowDialog();
            string path = "";
            if (dr == DialogResult.OK)
            {
                path = folderBrowserDialog.SelectedPath;
                try
                {
                    QuestionSet set = (QuestionSet)cbxSets.SelectedItem;
                    Stream setStream = File.Create(path + "\\" + set.ToString());
                    BinaryFormatter serializer = new BinaryFormatter();
                    serializer.Serialize(setStream, set);
                    setStream.Close();
                    MessageBox.Show("The set has been saved to the destination folder.");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("You must select a valid destination folder.");
                }
            }
            else
            {
                return;
            }
            this.Close();
        }
        private void Import()
        {
            string path = m.QuestionSetPath;
            string fullPath = path + "\\" + importedSet.ToString();
            try
            {
                if (File.Exists(fullPath))
                {
                    DialogResult dr = MessageBox.Show("A question set with the name '" + importedSet.ToString() + "' already exists.  Would you like to overwrite that set with the imported set?", "Overwriting", MessageBoxButtons.YesNo);
                    if (dr == DialogResult.No)
                    {
                        return;
                    }
                }
                Stream setStream = File.Create(fullPath);
                BinaryFormatter serializer = new BinaryFormatter();
                serializer.Serialize(setStream, importedSet);
                setStream.Close();
                MessageBox.Show("The set has been saved to the destination folder.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error has occured while saving the set to the question set folder.\n\n" + ex.ToString());
                return;
            }
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
