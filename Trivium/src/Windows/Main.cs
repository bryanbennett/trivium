﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Deployment.Application;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Trivium.src.Data;
using Trivium.src.Windows;

namespace Trivium
{
    public partial class Main : Form
    {
        private QuestionSet setToPlay;
        public Main()
        {
            InitializeComponent();
            if (Directory.Exists(QuestionSetPath))
            {
                Console.WriteLine("QuestionSets directory already exists.");
            }
            else
            {
                CreateFolders();
            }
        }
        private void CreateFolders()
        {
            DirectoryInfo di = Directory.CreateDirectory(QuestionSetPath);
            Console.WriteLine("QuestionSets directory was created.");

        }
        private void btnStart_Click(object sender, EventArgs e)
        {
            SelectSet ss = new SelectSet(this, true);
            DialogResult dr = ss.ShowDialog();
            if (dr == DialogResult.OK)
            {
                ss.Dispose();
                StartGame(setToPlay);
            }
            else
            {
                ss.Dispose();
            }
        }
        /// <summary>
        /// Sets the set to be played once the SelectSet dialog is returned.
        /// </summary>
        /// <param name="set"></param>
        public void SetQuestionSet(QuestionSet set)
        {
            setToPlay = set;
        }
        private void StartGame(QuestionSet set)
        {
            Game g = new Game(set, this);
            g.Show();
            this.Hide();
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            CreateSet create = new CreateSet(this);
            if (!create.IsDisposed)
            {
                create.ShowDialog();
                create.Dispose();
            }
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            SelectSet ss = new SelectSet(this, false);
            ss.ShowDialog();
            ss.Dispose();
        }
        private void btnAbout_Click(object sender, EventArgs e)
        {
            About about = new About();
            about.ShowDialog();
            about.Dispose();
        }
        private void btnImpExp_Click(object sender, EventArgs e)
        {
            ImportExport ie = new ImportExport(this);
            ie.ShowDialog();
            ie.Dispose();
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #region "Properties"
        /// <summary>
        /// Returns the executing directory of this running instance of Trivium.
        /// </summary>
        public string ExecutingPath { get { return Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName); } }

        public string QuestionSetPath { get { return ExecutingPath + "\\QuestionSets"; } }
        #endregion

        #region "MouseOver Buttons"
        private void btnStart_MouseHover(object sender, EventArgs e)
        {
            btnStart.BackColor = Color.FromArgb(0, 20, 20, 20);
        }

        private void btnCreate_MouseHover(object sender, EventArgs e)
        {
            btnCreate.BackColor = Color.FromArgb(0, 20, 20, 20);
        }

        private void btnOpen_MouseHover(object sender, EventArgs e)
        {
            btnOpen.BackColor = Color.FromArgb(0, 20, 20, 20);
        }

        private void btnAbout_MouseHover(object sender, EventArgs e)
        {
            btnAbout.BackColor = Color.FromArgb(0, 20, 20, 20);
        }

        private void btnExit_MouseHover(object sender, EventArgs e)
        {
            btnExit.BackColor = Color.FromArgb(0, 20, 20, 20);
        }

        private void btnImpExp_MouseHover(object sender, EventArgs e)
        {
            btnImpExp.BackColor = Color.FromArgb(0, 20, 20, 20);
        }
        #endregion
    }
}
